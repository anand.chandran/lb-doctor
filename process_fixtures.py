import json
import os

import click
import requests


@click.command()
@click.option('--algorithm-name', default='normal',
              help="LB Algorithm name to prepend to create processed directory"
              " (default='normal')")
@click.argument('fixture_location')
def process_fixtures(fixture_location, algorithm_name):
    """
    Given a folder of LineupBuilder input json files, run each one through
    LineupBuilder and save the output to the prefixed subdirectory
    """
    basename = os.path.basename(os.path.dirname(fixture_location))
    # construct output directory by prepending specified algorithm name
    output_location = f'{algorithm_name}_{basename}'
    print('Saving output to: ', output_location)

    # create output directory if needed
    if not os.path.exists(output_location):
        os.makedirs(output_location)

    LB_URL = 'http://localhost:3350/optimal'
    LINEUPS_PER_BUILD = 50

    # json files to feed to LineupBuilder
    filenames = [os.path.basename(f)
                 for f in os.listdir(fixture_location)
                 if f.endswith('.json')
                 ]

    # use session for repeated requests
    session = requests.Session()
    adapter = requests.adapters.HTTPAdapter(max_retries=3)
    session.mount('http://', adapter)

    for filename in filenames:
        with open(f'{fixture_location}/{filename}') as input_file:
            json_data = json.load(input_file)
            # modify outputCount as needed
            json_data['outputCount'] = LINEUPS_PER_BUILD
            print('Processing\n\t', filename)
            # POST input json to LineupBuilder
            r = session.post(LB_URL, json=json_data)
            output_data = r.json()
            # save LineupBuilder output json to file
            with open(f'{output_location}/{filename}', 'w') as output_file:
                json.dump(output_data, output_file)


if __name__ == '__main__':
    process_fixtures()
