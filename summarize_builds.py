import os
import json
import statistics

import click


def get_build_stats(build):
    """
    Return summary statistics for a given LineupBuilder build
    """
    lineups = build['lineups']

    projections = [lineup['projPts'] for lineup in lineups]
    salaries = [lineup['salary'] for lineup in lineups]

    mean_projection = statistics.mean(projections)
    mean_salary = statistics.mean(salaries)

    player_counts = get_player_counts(lineups)

    build_stats = {
            'num_lineups': len(lineups),
            'num_players': len(player_counts),
            'projection': {
                'max': max(projections),
                'mean': mean_projection,
                'median': statistics.median(projections),
                'min': min(projections),
                'stdev': statistics.stdev(projections, mean_projection),
                },
            'salaries': {
                'max': max(salaries),
                'mean': mean_salary,
                'median': statistics.median(salaries),
                'min': min(salaries),
                'stdev': statistics.stdev(salaries, mean_salary),
                },
            }

    build_stats['num_players'] = len(player_counts)
    return build_stats


def get_player_counts(lineups):
    """
    Return the number of times each player appears in a set of lineups
    """
    player_counts = {}
    for lineup in lineups:
        for position in lineup['positions']:
            player_id = position['player']
            # increment count if present or set to 1
            player_counts[player_id] = player_counts.get(player_id, 0) + 1
    return player_counts


@click.command()
@click.option('--summary-suffix', default='summarized',
              help="String to append to create summary json file"
              " (default='summarized')")
@click.argument('build_location')
def summarize_builds(build_location, summary_suffix):
    """
    Generate a summary json file from a directory of LineupBuilder builds

    Given LineupBuilder algorithm A, and a directory of builds created using A,
    this generated summary file represents some properties of algorithm A
    """
    basename = os.path.basename(os.path.dirname(build_location))
    # construct output filename by appending specified suffix
    summary_filename = f'{basename}_{summary_suffix}.json'
    # dictionary for build stats, keyed by filename
    summary_stats = {}
    print('Saving build summaries to: ', summary_filename)
    filenames = os.listdir(build_location)

    for filename in filenames:
        with open(f'./{build_location}/{filename}') as f:
            build = json.load(f)
            build_stats = get_build_stats(build)
            summary_stats[filename] = build_stats

    # write summary stats to constructed json file
    with open(summary_filename, 'w') as summary_file:
        json.dump(summary_stats, summary_file)


if __name__ == '__main__':
    summarize_builds()
