Usage:
-------------------------------------------------------------------------------
1. First generate input fixtures. Start by enabling request-saving on a locally
   running version of the LineupBuilder service.
2. Run **generate\_input\_fixtures.py**, with slate names and details in a
   configuration file (default: config.json). This will use browser automation
   to generate LineupBuilder requests in the same way a user would.
3. Take the saved requests (fixtures) from the LineupBuilder service and
   move them to a convenient place.
4. Now we are ready to test LineupBuilder algorithms. Suppose we have two
   LineupBuilder service branches, *branchA* and *branchB*. Check out *branchA*
   on the service and then run **process\_input\_fixtures.py**, specifying a
   short name to denote the current algorithm (say "branchA"). Check out
   *branchB* on the service and repeat the process, specifying the algorithm name.
5. Run **summarize\_builds.py** on each of the folders generated from step 4.
   This will create two json files, each containing a summary of *branchA* and
   *branchB* algorithms.
6. Run **compare\_summaries.py**, specifying the summary json files generated
   from step 5 to see how some basic metrics change from the *branchA*
   algorithm to the *branchB* algorithm.

Goals:
-------------------------------------------------------------------------------
Have more confidence when making core changes to the lineup generation
algorithms.

Create summary visualization and representative scores for build quality, as
defined by below metrics.

When comparing lineup builder algorithm A vs algorithm B, use these metrics to
determine if a new approach is better. These tests are NOT pass/fail, which
differentiates them from our standard tests.
(Final determination is subjective but can be made in a more informed manner.)

Add anonymized lb settings to test bucket. Take a random sample say n=100 of
these build settings, and run them through the test cases above to test
proposed algorithm changes.

Use machine learning to generate new test data, analyze data for trends to use
in future algorithm improvements.

Lineup Quality Metrics:
-------------------------------------------------------------------------------
0. Proper number of lineups are returned (stop testing upon failure)
1. Projected fpts of best lineup (median, quartiles etc)
2. Total number of players used (higher considered better)
3. Execution time (lower better)
4. Correlation/randomness measures (more random better)

Lineup Quality Test Cases:
-------------------------------------------------------------------------------
Note: Tests should be run with optimal/balanced/random for all appropriate
site/sport combinations

Slate sizes:
1. Medium/typical slate
2. Small slate
3. Large slate

Modification:
1. None/stock
2. Min players per team
3. Max players per team
4. High minLiked for lowly-projected player
5. Low maxLiked for highly-projected player
6. Add new player to pool via form
7. High total min exposures (greater than 100% for a given slot)

8. Exclude all of the player pool initially, add back interesting players
        Eg: For DK NFL we could take 10 RB, 15 WR, 3 QB, 5 TE
        For DK NBA: ~35 players
9. Stacking/groups for sports with this feature
10. Further tests mixing the above in different combinations
