import json

import click
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def load_config(filename):
    """
    Load file and parse JSON to dictionary
    """
    with open(filename) as json_data:
        return json.load(json_data)


def get_urls(config):
    """
    Return urls present in configuration dictionary
    """
    urls = []
    base_url = config['meta']['base_url']
    for site in config['slates']:
        for sport in config['slates'][site]:
            for slate_size in config['slates'][site][sport]:
                date = config['slates'][site][sport][slate_size]
                queryParams = f'site={site}&date={date}'
                url = f'http://{base_url}/{sport}?{queryParams}'
                urls.append(url)
    return urls


def generate_build(driver, url, strategy):
    """
    Navigate to LineupBuilder url, modify appropriate settings and click build
    """
    print('Generating fixture for:\n\t', url)
    driver.get(url)

    wait = WebDriverWait(driver, 10)
    reset_class_name = 'reset'
    wait.until(EC.element_to_be_clickable((By.CLASS_NAME, reset_class_name)))
    # reset storage to work around slate player exclusion issues
    driver.find_element_by_class_name(reset_class_name).click()

    wait.until(EC.alert_is_present())
    driver.switch_to.alert.accept()

    driver.execute_script('window.scrollTo(0, 0)')
    # turn the games filter on and off to ensure all players are in the pool
    toggle_games_filter(driver)

    # switch build strategy
    switch_strategy(driver, strategy)

    # change some settings to meet DK lineup requirements
    bypass_lock_like_block(driver)

    # prevent request splitting by lowering output count
    lower_output_count(driver)

    # generate the build
    driver.find_element_by_id('build-button').click()

    # wait until exposures table appears (successful build)
    exposures_selector = '.builder-exposures'
    wait.until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, exposures_selector))
        )


def lower_output_count(driver):
    """
    Change output count slider
    """
    output_count_selector = ('.lb-settings > .lb-settings-group >'
                             '.lb-setting > .slider > span')
    output_count_slider = driver.find_element_by_css_selector(
            output_count_selector
            )
    actions = webdriver.ActionChains(driver)
    actions.move_to_element(output_count_slider)
    actions.click()
    DEFAULT_LINEUPS = 10
    # threshold above which a request is split into multiple chunks
    CHUNK_THRESHOLD = 5
    # force slider to the left below threshold
    for i in range(DEFAULT_LINEUPS - CHUNK_THRESHOLD):
        actions.send_keys(Keys.LEFT)
    actions.perform()


def toggle_games_filter(driver):
    """
    Toggle the all games filter to force all players into pool
    """
    filter_expand_selector = '.lst.builder-tabs > .rt > a'
    # show games filter
    expand_link = driver.find_element_by_css_selector(filter_expand_selector)
    expand_link.send_keys(Keys.RETURN)

    games_filter_selector = '.lst.filters.multi.teams > .label.games'

    wait = WebDriverWait(driver, 10)
    wait.until(EC.element_to_be_clickable(
        (By.CSS_SELECTOR, games_filter_selector)))

    games_filter = driver.find_element_by_css_selector(games_filter_selector)
    # exclude and re-add all games to force all players in pool
    games_filter.click()

    # click a second time if invalid pool warning is displayed
    try:
        driver.find_element_by_css_selector('.blk.ntc.warning')
        games_filter.click()
    except NoSuchElementException:
        pass

    # hide games filter
    expand_link.send_keys(Keys.RETURN)


def switch_strategy(driver, strategy):
    """
    Switch between OPTIMAL, BALANCED, and RANDOM strategies
    """
    strategy_selector = ('.lb-strategy-selector > ul > li'
                         '> div > .slider > span')
    strategy_slider = driver.find_element_by_css_selector(strategy_selector)
    strategy_slider.click()

    # the number of times to move the strategy slider to the right
    num_right = 0
    if strategy == 'BALANCED':
        num_right = 1
    elif strategy == 'RANDOM':
        num_right = 2

    for i in range(num_right):
        strategy_slider.send_keys(Keys.RIGHT)


def bypass_lock_like_block(driver):
    """
    Give low salary players 99% maxLiked to bypass restriction
    """
    min_max_selector = ('.lb-settings > .lb-settings-group > .lst > li'
                        '> label > input[data-option-name=useMinMaxExposures]')
    driver.find_element_by_css_selector(min_max_selector).send_keys(Keys.SPACE)
    driver.execute_script('window.scrollTo(0, 0)')
    wait = WebDriverWait(driver, 10)
    salary_selector = 'div[data-for=tooltip-formatted_salary]'
    wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, salary_selector)))
    salary_col_header = driver.find_element_by_css_selector(salary_selector)

    # sort by lowest salary first
    salary_col_header.click()

    # modify maxLiked for lowest salary players in current pool view
    liked_selector = 'div[data-for=tooltip-maxLiked] + .editable-cell > input'
    first_max_liked_input = driver.find_element_by_css_selector(liked_selector)
    first_max_liked_input.send_keys('99')
    actions = webdriver.ActionChains(driver)
    actions.send_keys(Keys.TAB)
    actions.send_keys('99')
    actions.perform()

    # restore original sorting order
    salary_col_header.click()


@click.command()
@click.option('--config', default='./config.json',
              help='Path to configuration file')
@click.option('--driver', default='./browser_drivers/chromedriver',
              help='Path to browser driver')
def generate_fixtures(config, driver):
    """
    Generate LineupBuilder input fixtures through browser automation

    Enable LineupBuilder request-saving in the service before using this script
    """
    # load config from file to dict
    configuration = load_config(config)
    # extract target urls from configuration
    urls = get_urls(configuration)

    # run headless chrome for convenience
    browser_options = webdriver.ChromeOptions()
    browser_options.add_argument('start-maximized')
    # browser_options.add_argument('headless')

    print('Starting browser...')
    # sign in to basic access authentication for local dev
    browser_driver = webdriver.Chrome(driver, chrome_options=browser_options)
    try:
        username = configuration['meta']['username']
        password = configuration['meta']['password']
        base_url = configuration['meta']['base_url']
        browser_driver.get(f'http://{username}:{password}@{base_url}')

        strategies = ['OPTIMAL', 'BALANCED', 'RANDOM']
        for strategy in strategies:
            for url in urls:
                generate_build(browser_driver, url, strategy)
    finally:
        # clean up by closing browser
        browser_driver.quit()


if __name__ == '__main__':
    generate_fixtures()
